$(document).ready(function(){

	var color = $('#color');
	var body = $('body');
	var err = $('#error');
	var nombre = $('#nombre');

	color.change(function(){
		bgColor();
	})

	function bgColor(){
		body.css({
			'background-color': color.val()
		})
	}


	function validar(){
		if(nombre.val() == "" || nombre.val() == null){
			err.text('El nombre y apellido es obligatorio');
		}else{
			err.text('');
		}
	}

	$('.btn').click(function(){
		validar();
	})


})